//importarr librerias de angular
import { Component, OnInit , Input, HostBinding} from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.models';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
//declaracion de variable en typescript
// @Input() indica que la variable puede ser pasada como entrada al componente
@Input() nombre: string; 
@Input() destino: DestinoViaje;
//agregar un atributo al tag que representa al componente DestinoViajeComponent
 @HostBinding('attr.class') cssClass='col-md-4';
 

  constructor() { 
    this.nombre ="Destino viaje";
  }

  ngOnInit(): void {
  }

}
