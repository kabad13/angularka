export class DestinoViaje{
    nombre:string;
    imagenUrl:String;

    constructor(nombre:string, imagenUrl:String){
        this.nombre=nombre;
        this.imagenUrl=imagenUrl;
    }

}